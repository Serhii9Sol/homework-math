package tasks;

public class Tasks {

    public double gaubitsGrad(double a, double v){
        double aInRad = a * 3.14d / 180.0d;
        return gaubitsRad(aInRad,v);
    }

    public double gaubitsRad(double a, double v){
        if(v < 0){
            System.out.println("Отрицательная скорость");
            return -1d;
        }
        if(a < 0 || a > 3.14 / 2){
            System.out.println("Неправильный угол броска");
            return -1d;
        }
        double vInMetersOnSeconds = v / 3.6;
        double result = (2.0 * vInMetersOnSeconds * vInMetersOnSeconds * Math.sin(a) * Math.cos(a)) / 9.8;
        return result;
    }

    public double task2(double s, double v1, double v2, double t){
        if(s < 0 || v1 < 0 || v2 < 0 || t < 0){
            System.out.println("Отрицательные вводные даные");
            return -1.0d;
        }
        return s + (v1 + v2) * t;
    }

    public boolean task3(double x, double y){
        if(x >= y && x <= (2d*y/3d + 2d/3d) && x >= 0d){
            return true;
        }
        if(-x >= y && -x <= (2d*y/3d + 2d/3d) && x <= 0d){
            return true;
        }
        return false;
    }

    public Double task4(double x){
//        числа от [0пи до 1пи], [2пи до 3пи] и т.д. вне области существования функции
        double a = Math.exp(x + 1) + 2d * Math.exp(x) * Math.cos(x);
        double b = x - Math.exp(x + 1) * Math.sin(x);
        double rez = (6d * Math.log1p(Math.sqrt(a))) / (Math.log1p(b)) + Math.abs(Math.cos(x) / Math.exp(Math.sin(x)));
        return rez;
    }
}
