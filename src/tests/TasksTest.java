package tests;

import org.junit.jupiter.api.Test;
import tasks.Tasks;

import static org.junit.jupiter.api.Assertions.*;

class TasksTest {

    Tasks tasks = new Tasks();

    @Test
    void gaubitsGradTest1() {
        double expected = 0.787d;
        double actual = tasks.gaubitsGrad(45, 10d);
        assertTrue(Math.abs(expected - actual) < 0.001d);
    }

    @Test
    void gaubitsGradTest2() {
        double expected = -1d;
        double actual = tasks.gaubitsGrad(180, 10d);
        assertTrue(Math.abs(expected - actual) < 0.001d);
    }

    @Test
    void gaubitsGradTest3() {
        double expected = -1d;
        double actual = tasks.gaubitsGrad(45, -10d);
        assertTrue(Math.abs(expected - actual) < 0.001d);
    }

    @Test
    void gaubitsGradTest4() {
        double expected = 0d;
        double actual = tasks.gaubitsGrad(90, 10d);
        assertTrue(Math.abs(expected - actual) < 0.1d);
    }

    @Test
    void gaubitsRadTest1() {
        double expected = 0.787d;
        double actual = tasks.gaubitsRad(3.14/4, 10d);
        assertTrue(Math.abs(expected - actual) < 0.001d);
    }

    @Test
    void gaubitsRadTest2() {
        double expected = -1d;
        double actual = tasks.gaubitsRad(3.14, 10d);
        assertTrue(Math.abs(expected - actual) < 0.001d);
    }

    @Test
    void gaubitsRadTest3() {
        double expected = -1d;
        double actual = tasks.gaubitsRad(3.14/4, -10d);
        assertTrue(Math.abs(expected - actual) < 0.001d);
    }

    @Test
    void gaubitsRadTest4() {
        double expected = 0.0d;
        double actual = tasks.gaubitsRad(3.14/2, 10d);
        assertTrue(Math.abs(expected - actual) < 0.1d);
    }
//    ******************************************************

    @Test
    void task2Test1() {
        double expected = 30d;
        double actual = tasks.task2(5d, 10, 15, 1);
        assertTrue(Math.abs(expected - actual) < 0.1d);
    }

    @Test
    void task2Test2() {
        double expected = -1d;
        double actual = tasks.task2(5d, -10, 15, 1);
        assertTrue(Math.abs(expected - actual) < 0.1d);
    }
//    *********************************************
    @Test
    void task3Test1() {
        boolean expected = true;
        boolean actual = tasks.task3(0d,0d);
        assertTrue(expected == actual);
    }

    @Test
    void task3Test2() {
        boolean expected = true;
        boolean actual = tasks.task3(0.5d,0d);
        assertTrue(expected == actual);
    }

    @Test
    void task3Test3() {
        boolean expected = true;
        boolean actual = tasks.task3(-0.5d,0d);
        assertTrue(expected == actual);
    }

    @Test
    void task3Test4() {
        boolean expected = true;
        boolean actual = tasks.task3(0d,-0.5d);
        assertTrue(expected == actual);
    }

    @Test
    void task3Test5() {
        boolean expected = true;
        boolean actual = tasks.task3(-1d,0.5d);
        assertTrue(expected == actual);
    }
    @Test
    void task3Test6() {
        boolean expected = false;
        boolean actual = tasks.task3(-0.5d,-1d);
        assertTrue(expected == actual);
    }
    @Test
    void task3Test7() {
        boolean expected = false;
        boolean actual = tasks.task3(0.5d,-1d);
        assertTrue(expected == actual);
    }
//    *****************************************
    @Test
    void task4Test1() {
        assertTrue(tasks.task4(Math.PI - 0.1d).isNaN());
    }

    @Test
    void task4Test2() {
        assertTrue(!tasks.task4(Math.PI + 0.1d).isNaN());
    }

    @Test
    void task4Test3() {
        assertTrue(!tasks.task4(2 * Math.PI - 0.1d).isNaN());
    }

    @Test
    void task4Test4() {
        assertTrue(tasks.task4(2 * Math.PI + 0.1d).isNaN());
    }

    @Test
    void task4Test5() {
        double expected = 3.0d;
        double actual = tasks.task4(4.7d);
        assertTrue(Math.abs(expected - actual) < 0.1d);
    }
}